import { NavigationState } from '@react-navigation/routers';
export default function useOptionsGetters({ key, getOptions, getState, }: {
    key?: string;
    getOptions?: () => object | undefined;
    getState?: () => NavigationState;
}): {
    addOptionsGetter: (key: string, getter: () => object | null | undefined) => () => void;
    getCurrentOptions: () => object | null | undefined;
};
